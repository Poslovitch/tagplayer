package fr.poslovitch.tagplayer;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandTP implements CommandExecutor{
	
	TagPlayer main;
	
	public CommandTP(TagPlayer plugin){
		this.main = plugin;
		plugin.getCommand("tag").setExecutor(this);
	}
	
	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender send, Command cmd, String label, String[] args){
		
		if(args.length == 1 && args[0].equalsIgnoreCase("list")){
			if(!main.tagged.isEmpty()){
				send.sendMessage(main.tagList.replace("<nb>", String.valueOf(main.tagged.size())));
				String tag = ChatColor.GRAY + "";
				for(UUID uuid : main.tagged.keySet()){
					String name = Bukkit.getServer().getOfflinePlayer(uuid).getName();
					if(tag.equals(ChatColor.GRAY + ""))
						tag += name;
					else
						tag += ", " + name;
				}
				send.sendMessage(tag);
			}
			
			else
				send.sendMessage(main.noTagged);
		}
		
		else if(args.length == 2){
			String psd = args[1].toString();
			OfflinePlayer target = Bukkit.getServer().getOfflinePlayer(psd);
			UUID uuid = target.getUniqueId();

			if(args[0].equalsIgnoreCase("remove")){
				if(main.tagged.containsKey(uuid)){
					main.tagged.remove(uuid);
					send.sendMessage(main.tagRemove.replace("<pseudo>", psd));
				}

				else
					send.sendMessage(main.isNotTagged);
			}

			if(args[0].equalsIgnoreCase("info")){
				if(main.tagged.containsKey(uuid)){
					String message = main.tagInfo.replace("<pseudo>", psd).replace("<raison>", main.tagged.get(uuid));
					send.sendMessage(message);
				}

				else
					send.sendMessage(main.isNotTagged);
			}
		}
		
		else if(args.length >= 3){
			String psd = args[1].toString();
			OfflinePlayer target = Bukkit.getServer().getOfflinePlayer(psd);
			UUID uuid = target.getUniqueId();
			
			if(args[0].equalsIgnoreCase("add")){
				if(target.hasPlayedBefore()){
					if(!main.tagged.containsKey(uuid)){
						String raison = "";
						for(int i = 2; i < args.length; i++){
							raison += args[i] + " ";
						}

						main.tagged.put(uuid, raison);
						send.sendMessage(main.tagAdd.replace("<pseudo>", psd).replace("<raison>", raison));
					}

					else
						send.sendMessage(main.isTagged);
				}

				else
					send.sendMessage(main.notPlayedBefore.replace("<pseudo>", psd));
			}
			
			if(args[0].equalsIgnoreCase("modify")){
				if(main.tagged.containsKey(uuid)){
					String raison = "";
					for(int i = 2; i < args.length; i++){
						raison += args[i] + " ";
					}
					main.tagged.put(uuid, raison);
					send.sendMessage(main.tagNewRaison.replace("<pseudo>", psd).replace("<raison>", raison));
				}

				else
					send.sendMessage(main.isNotTagged);
			}
		}
		
		else
			send.sendMessage(main.tagHelp);
		
		return true;
	}
}
