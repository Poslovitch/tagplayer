package fr.poslovitch.tagplayer;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class TagPlayer extends JavaPlugin{

	protected HashMap<UUID, String> tagged = new HashMap<UUID, String>();

	protected String arrow = ChatColor.GOLD + "> " + ChatColor.DARK_GREEN;
	protected String tag = ChatColor.GRAY + "[" + ChatColor.RED + "Tag" + ChatColor.GRAY + "] ";
	protected String[] tagHelp = {ChatColor.AQUA + "Syntaxe" + ChatColor.GRAY + " /tag " + ChatColor.AQUA + ":", arrow + "/tag list", arrow + "/tag add <pseudo> <raison>", arrow + "/tag remove <pseudo>", arrow + "/tag info <pseudo>", arrow + "/tag modify <pseudo> <raison>"};
	protected String tagList = ChatColor.GREEN + "Liste des joueurs tagg�s " + ChatColor.GRAY + "(" + ChatColor.AQUA + "<nb>" + ChatColor.GRAY + ")" + ChatColor.GREEN + " :";
	protected String isTagged = tag + ChatColor.RED + "Ce joueur est d�j� tagg�!";
	protected String isNotTagged = tag + ChatColor.RED + "Ce joueur n'est pas tagg�!";
	protected String tagInfo = tag + ChatColor.GOLD + "<pseudo>" + ChatColor.GRAY + " | " + ChatColor.BLUE + "<raison>";
	protected String tagRemove = tag + ChatColor.RED + "Retir� de la liste : " + ChatColor.GOLD + "<pseudo>";
	protected String tagAdd = tag + ChatColor.GREEN + "Enregistr� dans la liste : " + ChatColor.GOLD + "<pseudo>" + ChatColor.GRAY + " | " + ChatColor.BLUE + "<raison>";
	protected String tagNewRaison = tag + ChatColor.GREEN + "Nouvelle raison pour " + ChatColor.GOLD + "<pseudo>" + ChatColor.GREEN + " : " + ChatColor.BLUE + "<raison>";
	protected String tagNeedRaison = tag + ChatColor.RED + "Vous devez sp�cifier une raison !";
	protected String noTagged = tag + ChatColor.RED + "Aucun joueur n'est tagg� !";
	protected String notPlayedBefore = tag + ChatColor.GOLD + "<pseudo>" + ChatColor.RED + " n'est jamais venu sur le serveur";


	File taggedPlayersFile = new File("plugins/TagPlayer/taggedPlayers.yml");
	FileConfiguration taggedPlayers = YamlConfiguration.loadConfiguration(taggedPlayersFile);

	@Override
	public void onEnable(){

		if(!taggedPlayersFile.exists()){
			try {
				taggedPlayersFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


		loadTaggedPlayers();

		new EventTP(this);
		new CommandTP(this);
	}

	@Override
	public void onDisable(){
		saveConfig();
		saveTaggedPlayers();
	}

	public void loadTaggedPlayers(){

		if(taggedPlayers.getConfigurationSection("taggedPlayers") != null){
			for (String s : taggedPlayers.getConfigurationSection("taggedPlayers").getKeys(false)){
				tagged.put(UUID.fromString(s), taggedPlayers.getString("taggedPlayers." + s));
			}
			getLogger().info(tagged.size() + " joueurs charg�s");
		}

		else
			getLogger().info("Aucun joueurs charg�s");
	}

	public void saveTaggedPlayers(){
		taggedPlayers.set("taggedPlayers", null);

		for(Entry<UUID, String> e : tagged.entrySet()){
			taggedPlayers.set("taggedPlayers." + e.getKey().toString(), e.getValue());
		}
		try {
			taggedPlayers.save(taggedPlayersFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
