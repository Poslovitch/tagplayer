package fr.poslovitch.tagplayer;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EventTP implements Listener{
	
	TagPlayer main;
	
	public EventTP(TagPlayer main){
		this.main = main;
		main.getServer().getPluginManager().registerEvents(this, main);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		UUID uuid = p.getUniqueId();
		
		if(main.tagged.containsKey(uuid)){
			String raison = main.tagged.get(uuid);
			
			String tag = main.tag + ChatColor.GOLD + p.getName() + ChatColor.GRAY + " | " + ChatColor.BLUE + raison;
			for(Player op : Bukkit.getOnlinePlayers()){
				if(op.isOp() || op.hasPermission("tagplayer.alert")){
					op.sendMessage(tag);
					op.playSound(op.getLocation(), Sound.ANVIL_LAND, 1.0F, 1.0F);
				}
			}
		}
	}
}
